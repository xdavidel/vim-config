filetype plugin indent on
syntax on

set backspace=indent,eol,start
set hidden
set number relativenumber
set noswapfile
set colorcolumn=80

let mapleader=" "
let $RTP=split(&runtimepath, ',')[0]
let $RC="$HOME/.vim/vimrc"

nnoremap <leader>n :nohl<CR>

set path=.,**
